# Randomiser Plugin

This is a wordpress plugin which allows you you to make a list of items that you can display, then a button to randomly select an item from the list. 

Items may include an optional featured image. 

After you have added the items using the "Randomiser List" section in the admin area, you can use the shortcode [sjf_randomiser] using the shortcode block to display on the front end.

Admin area:

![picture](https://bitbucket.org/sf-repos/wordpress-random-list-plugin/raw/master/screenshots/admin.png)

Editing an entry:

![picture](https://bitbucket.org/sf-repos/wordpress-random-list-plugin/raw/master/screenshots/edit.png)

Shortcode required:

![picture](https://bitbucket.org/sf-repos/wordpress-random-list-plugin/raw/master/screenshots/shortcode.png)

How it looks on the front end:

![picture](https://bitbucket.org/sf-repos/wordpress-random-list-plugin/raw/master/screenshots/frontend.png)