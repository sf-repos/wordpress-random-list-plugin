<?php
/*
Plugin Name: Randomiser
Plugin URI: https://bitbucket.org/sf-repos/wordpress-random-list-plugin
Description: Display a random item from the custom post type ("Randomiser List") included in this plugin.
Version: 1.0
Author: Sam Fullalove
Author URI: http://sam.fullalove.co
Text Domain: sjf-randomiser
License: GPLv2
*/

/*
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
USA
*/


/**
* Run this code when plugin is activated
*/
function sjf_randomiser_activation() {
  global $wp_version;

  //make sure running minimum wordpress version (that I've tested) exists to use correct wp functions
  if ( version_compare( $wp_version, '4.1', '<' ) ) {
  wp_die( 'This plugin requires WordPress version 5.2.2 or higher.' );

  }
}
register_activation_hook( __FILE__, 'sjf_randomiser_activation' );

/**
 * Enqueue css
 */
function sjf_randomiser_css() {
  wp_register_style( 'sjf_randomiser_css', plugin_dir_url( __FILE__ ) . 'css/style.css', false, '1.0.0' );
  wp_enqueue_style( 'sjf_randomiser_css' );
}
add_action( 'wp_enqueue_scripts', 'sjf_randomiser_css', 11 );

/**
* Make the RandomiserList custom post type
*/ 
function sjf_randomiser_create_posttype() {
 
  register_post_type( 'RandomiserList',
  // CPT Options
      array(
          'labels' => array(
              'name' => __( 'Randomiser List' ),
              'singular_name' => __( 'Randomiser List Item' )
          ),
          'supports' => array(
            'title',
            'thumbnail'
        ),
          'public' => true,
          'has_archive' => true,
          'rewrite' => array('slug' => 'RandomiserList'),
      )
  );
}
// Hook for custom post type function
add_action( 'init', 'sjf_randomiser_create_posttype' );

/**
* [sjf_randomiser] Shortcode for displaying all of the frontend html and js
*/
function sjf_randomiser_shortcode(){

  $args = array(
    'post_type' => 'RandomiserList',
    'post_status' => 'publish',
    'posts_per_page' => -1,
);
$posts = new WP_Query( $args );
$randomiserItems = array(
  array (
    "title" => '',
    "image" => ''
  )
);

$x = 0;
while ($posts->have_posts()) : $posts->the_post();

$randomiserItems[$x]['title'] = get_the_title();
$randomiserItems[$x]['image'] = get_the_post_thumbnail($posts->ID, 'medium');

$x++;

endwhile;

wp_reset_postdata();


  return '
      <script>

      function sjf_randomiser_display()
      {
          var randomiserItems = '.json_encode( $randomiserItems ).';
          var randomItem = randomiserItems[Math.floor(Math.random() * randomiserItems.length)];

          document.getElementById("randomiserListTitle").innerHTML = randomItem["title"];
          document.getElementById("randomiserListImage").innerHTML = randomItem["image"];
      }
      </script>

      <div id="randomiserListDiv">
          <span id="randomiserListTitle">Click The button!</span>
          <br/> <br/>
          <div id="randomiserListImage"></div>
          <br/>
          <input id="clickMe" type="button" value="Click Me" onclick="sjf_randomiser_display();" />

      </div>
  ';

}
add_shortcode( 'sjf_randomiser', 'sjf_randomiser_shortcode' );